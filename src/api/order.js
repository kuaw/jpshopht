import request from '@/utils/request'

//订单管理

/**
 * 获取订单
 * @param {*} params
 */
export function getOrder(params) {
  return request({
    url: '/merchantOrder',
    method: 'get',
    params
  })
}
/**
 * 获取订单退款详情
 * @param {*} params
 */
export function getSuborder(params) {
  return request({
    url: '/merchantSuborder',
    method: 'get',
    params
  })
}
/**
 * 同意退款，修改状态
 * @param {*} params
 */
export function putOrderAfter(params) {
  return request({
    url: '/merchantOrderAfter',
    method: 'put',
    data: params
  })
}
/**
 * 同意退款，修改状态，退款退货或退款
 * @param {*} params
 */
export function putOrderAfterStatus(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantOrderAfter/'+id,
    method: 'put',
    data: params
  })
}

/**
 * 修改订单发货状态
 * @param {*} params
 */
export function putOrderStatus(params) {
  return request({
    url: '/merchantSend',
    method: 'put',
    data: params
  })
}
/**
 * UU跑腿发货后修改订单发货状态
 * @param {*} params
 */
export function getStatusUU(params) {
  return request({
    url: '/merchantUuGetorderprice',
    method: 'post',
    data: params
  })
}
/**
 * UU跑腿发货后修改订单发货状态
 * @param {*} params
 */
export function putOrderStatusUU(params) {
  return request({
    url: '/merchantUuAddorder',
    method: 'post',
    data: params
  })
}
/**
 * 点我达发货后修改订单发货状态
 * @param {*} params
 */
export function putOrderStatusDianwoda(params) {
  return request({
    url: '/dianwodaCreate',
    method: 'post',
    data: params
  })
}
/**
 * 添加修改订单备注
 * @param {*} params
 */
export function putOrderRemark(params) {
  return request({
    url: '/merchantOrderRemark',
    method: 'put',
    data: params
  })
}
/**
 * 一键退款
 * @param {*} params
 */
export function putOrderRefund(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantOrderRefund/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 打印小票
 * @param {*} params
 */
export function print(params) {
  return request({
    url: '/merchantPrint',
    method: 'get',
    params
  })
}
/**
 * 获取自提点信息
 * @param {*} params
 */
export function getTakeTheir(params) {
  return request({
    url: '/merchantTuanUser',
    method: 'get',
    params
  })
}
/**
 * 修改订单中的自提点信息
 * @param {*} params
 */
export function putOrderLeader(params) {
  return request({
    url: '/merchantOrderLeader',
    method: 'put',
    data: params
  })
}


//订单概述

/**
 * 获取订单概述的数据
 * @param {*} params
 */
export function getOrderSummary(params) {
  return request({
    url: '/merchantOrderSummary',
    method: 'get',
    params
  })
}


//评价管理

/**
 * 获取评价列表
 * @param {*} params
 */
export function getOrderComment(params) {
  return request({
    url: '/merchantComment',
    method: 'get',
    params
  })
}
/**
 * 修改评价
 * @param {*} params
 */
export function putOrderComment(params) {
  const id = params.id;
  delete params.id
  return request({
    url: '/merchantComment/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除评价
 * @param {*} params
 */
export function delOrderComment(params) {
  const id = params.id;
  delete params.id
  return request({
    url: '/merchantComment/'+id,
    method: 'delete',
    data: params
  })
}
