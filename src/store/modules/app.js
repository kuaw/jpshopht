import Cookies from 'js-cookie'
import { merchantApp } from '@/api/app'
import { getActiveApp, setActiveApp } from '@/utils/auth'

const state = {
  sidebar: {
    opened: Cookies.get('sidebarStatus') ? !!+Cookies.get('sidebarStatus') : true,
    withoutAnimation: false
  },
  device: 'desktop',
  merchantApp: [],
  activeApp: getActiveApp() == null || getActiveApp() == undefined || getActiveApp() == '' ? '' : JSON.parse(getActiveApp())
}

const mutations = {
  TOGGLE_SIDEBAR: state => {
    state.sidebar.opened = !state.sidebar.opened
    state.sidebar.withoutAnimation = false
    if (state.sidebar.opened) {
      Cookies.set('sidebarStatus', 1)
    } else {
      Cookies.set('sidebarStatus', 0)
    }
  },
  CLOSE_SIDEBAR: (state, withoutAnimation) => {
    Cookies.set('sidebarStatus', 0)
    state.sidebar.opened = false
    state.sidebar.withoutAnimation = withoutAnimation
  },
  TOGGLE_DEVICE: (state, device) => {
    state.device = device
  },
  MerchantApp: (state, value) => {
    state.merchantApp = value
  },
  ActiveApp: (state, value) => {//激活的应用
    state.activeApp = value
  }
}

const actions = {
  toggleSideBar({ commit }) {
    commit('TOGGLE_SIDEBAR')
  },
  closeSideBar({ commit }, { withoutAnimation }) {
    commit('CLOSE_SIDEBAR', withoutAnimation)
  },
  toggleDevice({ commit }, device) {
    commit('TOGGLE_DEVICE', device)
  },
  //应用列表
  setMerchantApp({ commit }) {
    return new Promise((resolve, reject) => {
      merchantApp().then(response => {
        const { data } = response
        commit('MerchantApp',data)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },
  //激活应用
  SetActiveApp({ commit }, value) {
    setActiveApp(value)
    commit('ActiveApp',value)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
