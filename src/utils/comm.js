/**
 * 图片转base64 貌似return不了，暂时哪里用就复制到哪里吧
 * @param {*} file 
 */
export function imageToBase64(file) {
  if (window.FileReader) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = function (e) {
      return e.target.result;
    };
  }
}/**
 * 时间戳转换
 * @param {*} row
 */
export function formatDate(row) {
  const date = new Date(parseInt(row) * 1000);
  const Y = date.getFullYear() + '-';
  const M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
  const D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
  const h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
  const m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
  const s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
  return Y + M + D + h + m + s;
}